// pages/finished/finished.js
import rawChoiceSubjectList from '../../data/choiceSubjectList'
import rawCompleteSubjectList from '../../data/completeSubjectList'
import rawJudgeSubjectList from '../../data/judgeSubjectList'

import {subjectList} from '../../data/subjectLIst'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    answeredSubjectList: [],
    correctAnswerAmount: 0,
    // A: 'A',
    // B: 'B',
    // C: 'C',
    // D: 'D',
    // https://developers.weixin.qq.com/miniprogram/dev/component/radio.html
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const randomSubjectListJson = options.randomSubjectListJson;
    const answeredSubjectList = JSON.parse(randomSubjectListJson);
    const correctAnswerAmount = answeredSubjectList.filter(subject=>subject.answer===subject.userAnswer).length;
    this.setData({answeredSubjectList, correctAnswerAmount});
  },

  handleModifyBtnClick(){
    wx.navigateBack({
      delta: 1,
    })
  },

  handleOncemoreBtnClick(){
    wx.reLaunch({
      url: '/pages/home/home',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})