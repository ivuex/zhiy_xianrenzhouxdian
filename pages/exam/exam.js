import rawChoiceSubjectList from '../../data/choiceSubjectList'
import rawCompleteSubjectList from '../../data/completeSubjectList'
import rawJudgeSubjectList from '../../data/judgeSubjectList'

// pages/exam/exam.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    randomChoiceSubjectList: [],
    randomCompleteSubjectList: [],
    randomJudgeSubjectList: [],
    choiceAmount: 3,
    completeAmount: 3,
    judgeAmount: 3,
    // https://developers.weixin.qq.com/miniprogram/dev/component/radio.html
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // const randomSubjectList = this.getRandomSubjectList();
    const randomChoiceSubjectList = this.getRandomChoiceSubjectList();
    const randomCompleteSubjectList = this.getRandomCompleteSubjectList();
    const randomJudgeSubjectList = this.getRandomJudgeSubjectList();
    console.log(randomChoiceSubjectList);
    console.log(randomCompleteSubjectList);
    console.log(randomJudgeSubjectList);
    this.setData({
      // randomSubjectList,
      randomChoiceSubjectList,
      randomCompleteSubjectList,
      randomJudgeSubjectList,
    });
  },

  // getRandomSubjectList(){
  //   const randomSubjectList = [];
  //   const tmpSubjectList = [...rawSubjectList];
  //   for(let i=0; i<5; i++){
  //     const subjectIndex = parseInt(Math.random()*tmpSubjectList.length);
  //     const subject = tmpSubjectList.splice(subjectIndex, 1)[0];
  //     subject['userAnswer'] = '未作答';
  //     randomSubjectList.push(subject);
  //   }
  //   return randomSubjectList;
  // },


  getRandomChoiceSubjectList(){
    const randomChoiceSubjectList = [];
    const tmpChoiceSubjectList = [...rawChoiceSubjectList];
    for(let i=0; i<this.data.choiceAmount; i++){
      const choiceSubjectIndex = parseInt(Math.random()*tmpChoiceSubjectList.length);
      const subject = tmpChoiceSubjectList.splice(choiceSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomChoiceSubjectList.push(subject);
    }
    return randomChoiceSubjectList;
  },

  getRandomJudgeSubjectList(){
    const randomJudgeSubjectList = [];
    const tmpJudgeSubjectList = [...rawJudgeSubjectList];
    for(let i=0; i<this.data.judgeAmount; i++){
      const judgeSubjectIndex = parseInt(Math.random()*tmpJudgeSubjectList.length);
      const subject = tmpJudgeSubjectList.splice(judgeSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomJudgeSubjectList.push(subject);
    }
    return randomJudgeSubjectList;
  },


  getRandomCompleteSubjectList(){
    const randomCompleteSubjectList = [];
    const tmpCompleteSubjectList = [...rawCompleteSubjectList];
    for(let i=0; i<this.data.completeAmount; i++){
      const completeSubjectIndex = parseInt(Math.random()*tmpCompleteSubjectList.length);
      const subject = tmpCompleteSubjectList.splice(completeSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomCompleteSubjectList.push(subject);
    }
    return randomCompleteSubjectList;
  },

  answerValueChange(e) {
    // let name = e.currentTarget.dataset.name;
    const curRandomSubjectIndex = e.currentTarget.dataset.subjectindex;
    let type = e.currentTarget.dataset.type;
    type = type[0].toUpperCase() + type.slice(1);
    const userAnswer = e.detail.value.replace(/\s/g, '');
    const dynamicKey = `random${type}SubjectList[${curRandomSubjectIndex}].userAnswer`;
    this.setData({
      [dynamicKey]: userAnswer,
      // [name]: e.detail.value,  
    })
  },

  handleSubmitBtnClick(){
    // console.log(`46-46 randomSubjectListJson: ${randomSubjectListJson}`);
    if(this.data.randomChoiceSubjectList.some(item=>!item.userAnswer)){
      wx.showToast({
        icon: 'none',
        title: '选择题没有做完，您不能交卷',
      })
      return;
    }
    if(this.data.randomJudgeSubjectList.some(item=>!item.userAnswer)){
      wx.showToast({
        icon: 'none',
        title: '判断题没有做完，您不能交卷',
      })
      return;
    }
    if(this.data.randomCompleteSubjectList.some(item=>!item.userAnswer)){
      wx.showToast({
        icon: 'none',
        title: '填空题没有做完，您不能交卷',
      })
      return;
    }
    const randomChoiceSubjectListJson = JSON.stringify(this.data.randomChoiceSubjectList);
    const randomCompleteSubjectListJson = JSON.stringify(this.data.randomCompleteSubjectList);
    const randomJudgeSubjectListJson = JSON.stringify(this.data.randomJudgeSubjectList);
    const url = `/pages/finished/finished?randomChoiceSubjectListJson=${randomChoiceSubjectListJson}&randomCompleteSubjectListJson=${randomCompleteSubjectListJson}&randomJudgeSubjectListJson=${randomJudgeSubjectListJson}`;
    console.log('130-130', url);
    //  return;
    wx.navigateTo({
      url,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})