
// pages/exam/exam.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    randomChoiceSubjectList: [],
    randomCompleteSubjectList: [],
    randomJudgeSubjectList: [],
    correctChoiceAmount: 0,
    correctCompleteAmount: 0,
    correntJudgeAmount: 0,
    commentWordsDict: {
      '3': '全都答对了，继续加油哟',
      '2': '正确率较高，争取全对吧',
      '1': '正确率一般，有待提高哦',
      '0': '正确率较低，多多练习呗',
    },
    choiceComment: '',
    judgeComment: '',
    completeComment: '',
    // https://developers.weixin.qq.com/miniprogram/dev/component/radio.html
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // const randomSubjectList = this.getRandomSubjectList();
    const randomChoiceSubjectList = JSON.parse(options.randomChoiceSubjectListJson);
    const randomJudgeSubjectList = JSON.parse(options.randomJudgeSubjectListJson);
    const randomCompleteSubjectList = JSON.parse(options.randomCompleteSubjectListJson);
    console.log(randomChoiceSubjectList);
    console.log(randomCompleteSubjectList);
    console.log(randomJudgeSubjectList);
    let correctChoiceAmount = randomChoiceSubjectList.filter(item => item.answer === item.userAnswer).length;
    const choiceComment = this.data.commentWordsDict[correctChoiceAmount.toString()];
    let correctJudgeAmount = randomJudgeSubjectList.filter(item => item.answer === item.userAnswer).length;
    const judgeComment = this.data.commentWordsDict[correctJudgeAmount.toString()];
    let correctCompleteAmount = randomCompleteSubjectList.filter(item => item.answer === item.userAnswer).length;
    const completeComment = this.data.commentWordsDict[correctCompleteAmount.toString()];
    this.setData({
      // randomSubjectList,
      randomChoiceSubjectList,
      randomCompleteSubjectList,
      randomJudgeSubjectList,
      correctChoiceAmount,
      correctJudgeAmount,
      correctCompleteAmount,
      choiceComment,
      judgeComment,
      completeComment,
    });
  },

  // getRandomSubjectList(){
  //   const randomSubjectList = [];
  //   const tmpSubjectList = [...rawSubjectList];
  //   for(let i=0; i<5; i++){
  //     const subjectIndex = parseInt(Math.random()*tmpSubjectList.length);
  //     const subject = tmpSubjectList.splice(subjectIndex, 1)[0];
  //     subject['userAnswer'] = '未作答';
  //     randomSubjectList.push(subject);
  //   }
  //   return randomSubjectList;
  // },


  getRandomChoiceSubjectList() {
    const randomChoiceSubjectList = [];
    const tmpChoiceSubjectList = [...rawChoiceSubjectList];
    for (let i = 0; i < this.data.choiceAmount; i++) {
      const choiceSubjectIndex = parseInt(Math.random() * tmpChoiceSubjectList.length);
      const subject = tmpChoiceSubjectList.splice(choiceSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomChoiceSubjectList.push(subject);
    }
    return randomChoiceSubjectList;
  },

  getRandomJudgeSubjectList() {
    const randomJudgeSubjectList = [];
    const tmpJudgeSubjectList = [...rawJudgeSubjectList];
    for (let i = 0; i < this.data.judgeAmount; i++) {
      const judgeSubjectIndex = parseInt(Math.random() * tmpJudgeSubjectList.length);
      const subject = tmpJudgeSubjectList.splice(judgeSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomJudgeSubjectList.push(subject);
    }
    return randomJudgeSubjectList;
  },


  getRandomCompleteSubjectList() {
    const randomCompleteSubjectList = [];
    const tmpCompleteSubjectList = [...rawCompleteSubjectList];
    for (let i = 0; i < this.data.completeAmount; i++) {
      const completeSubjectIndex = parseInt(Math.random() * tmpCompleteSubjectList.length);
      const subject = tmpCompleteSubjectList.splice(completeSubjectIndex, 1)[0];
      subject['userAnswer'] = '';
      randomCompleteSubjectList.push(subject);
    }
    return randomCompleteSubjectList;
  },

  answerValueChange(e) {
    // let name = e.currentTarget.dataset.name;
    const curRandomSubjectIndex = e.currentTarget.dataset.subjectindex;
    let type = e.currentTarget.dataset.type;
    type = type[0].toUpperCase() + type.slice(1);
    const userAnswer = e.detail.value;
    const dynamicKey = `random${type}SubjectList[${curRandomSubjectIndex}].userAnswer`;
    this.setData({
      [dynamicKey]: userAnswer,
      // [name]: e.detail.value,  
    })
  },

  handleSubmitBtnClick() {
    const randomChoiceSubjectListJson = JSON.stringify(this.data.randomChoiceSubjectList);
    const randomCompleteSubjectListJson = JSON.stringify(this.data.randomCompleteSubjectList);
    const randomJudgeSubjectListJson = JSON.stringify(this.data.randomJudgeSubjectList);
    // console.log(`46-46 randomSubjectListJson: ${randomSubjectListJson}`);
    wx.navigateTo({
      url: `/pages/finished/finished?randomChoiceSubjectListJson=${randomChoiceSubjectListJson}&randomCompleteSubjectListJson=${randomCompleteSubjectListJson}&randomJudgeSubjectListJson=${randomJudgeSubjectListJson}`,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})