export default [

  {
    type: 'judge',
    question: '1的倒数是1，0的倒数是0。',
    answer: '×',	
    yes: '√',
    no: '×',
  },

  {
    type: 'judge',
    question: '1米的3/8和3米的1/8一样长。',
    answer: '√',	
    yes: '√',
    no: '×',	
  },
  
  {
    type: 'judge',
    question: '正方体和长方体都可以用底面积乘相应的高来求它们的体积。',
    answer: '√',	
    yes: '√',
    no: '×',	
  },

  {
    type: 'judge',
    question: '六年级两个班周五的出勤情况是：一班出勤率100％，二班出勤率为98％。由此看来一班出勤的人数多。',
    answer: '×',	
    yes: '√',
    no: '×',	
  },
  {
    type: 'judge',
    question: '一次英语测试，六年一班40名学生，2人不及格，及格率是98%。',
    answer: '×',	
    yes: '√',
    no: '×',	
  },
  
  {
    type: 'judge',
    question: '一个长方体相交于一个顶点的三条棱长总和是15厘米，这个长方体的棱长总和60厘米。',
    answer: '√',	
    yes: '√',
    no: '×',	
  },

  {
    type: 'judge',
    question: '正方体是特殊的长方体。',
    answer: '√',	
    yes: '√',
    no: '×',	
  },

  {
    type: 'judge',
    question: '小美班有45人，今天全部出勤，出勤率是100％。',
    answer: '√',	
    yes: '√',
    no: '×',
  },

  {
    type: 'judge',
    question: '一个数乘整数，积一定大于这个数。',
    answer: '×',	
    yes: '√',
    no: '×',	
  },

  {
    type: 'judge',
    question: '甲、乙两班男生各占本班人数的4/7，则甲、乙两班男生人数相等。',
    answer: '×',	
    yes: '√',
    no: '×',
  },

];
