export default [

  {
    type: 'complete',
    question: '从____统计图中能清楚地看出各部分量占总量的百分比。',
    answer: '扇形',	
  },

  {
    type: 'complete',
    question: '将4克盐溶解在46克水中，所得盐水的含盐率是____％。',
    answer: '40',	
  },
  
  {
    type: 'complete',
    question: '一件上衣原价300元，现按八五折出售，应卖____元。',
    answer: '255',	
  },

  {
    type: 'complete',
    question: '正方体的棱长扩大为原来的2倍，体积变为原来的____倍。',
    answer: '8',	
  },
  {
    type: 'complete',
    question: '蜂蜜中糖分占3/4，20克蜂蜜中，含糖____克。',
    answer: '15',	
  },
  
  {
    type: 'complete',
    question: '一件衣服打七折后是35元，原价是____元。',
    answer: '50',	
  },

  {
    type: 'complete',
    question: '把4/7平均分成3份，也就是求4/7的____是多少。',
    answer: '1/3',	
  },

  {
    type: 'complete',
    question: '一堆沙子，每天用去2/5，2天用去____。',
    answer: '4/5'
  },

  {
    type: 'complete',
    question: '一根绳子长12米，它的一半是____米。',
    answer: '6',	
  },

];
