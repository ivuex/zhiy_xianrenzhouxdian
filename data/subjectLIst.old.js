export const subjectList = [
{
  question: '在以下人为的恶意攻击行为中，属于主动攻击的是',
  answer: 'A',	
  A: '身份假冒',
  B: '数据GG',
  C: '数据流分析',
  D: '非法访问',
},
{
  question: '防止用户被冒名所欺骗的方法是',
  answer: 'A',
  A: '对信息源发送方进行身份验证',
  B: '进行数据加密',
  C: '对访问网络的流量进行过滤和保护',
  D: '采用防火墙',
},
{
  question: '以下关于VPN说法正确的是',
  answer: 'B',
  A: '指的是用户自己租用路线，和公共网络物理上完全隔离的，安全的路线',
  B: '指的是用户通过公用网络建立的临时的，安全的连接',
  C: '不能做到信息认证和身份认证',
  D: '只能提供身份认证，不能提供加密数据的功能',
},
{
  question: '防火墙中地址翻译的主要作用是',
  answer: 'B',
  A: '提供代理服务',
  B: '隐藏内部网络地址',
  C: '进行入侵检测',
  D: '防止病毒入侵',
},
{
  question: '对称密钥加密技术的特点是什么',
  answer: 'A',
  A: '无论加密还是解密都用同一把密钥',
  B: '收信方和发信方使用的密钥互不相同',
  C: '不能从加密密钥推导解密密钥',
  D: '可以适应网络的开放性要求',
},
{
  question: '屏蔽主机式防火墙体系结构的优点是什么',
  answer: 'A',
  A: '此类型防火墙的安全级别较高',
  B: '如果路由表遭到破坏，则数据包会路由到堡垒主机上',
  C: '使用此结构，必须关闭双网主机上的路由分配功能',
  D: '此类型防火墙结构简单，方便部署',
},
{
  question: '企事业单位的网络环境中应用安全审计系统的目的是什么',
  answer: 'D',
  A: '为了保障企业内部信息数据的完整性',
  B: '为了保障企业业务系统不受外部威胁攻击',
  C: '为了保障网络环境不存在安全漏洞，感染病毒',
  D: '为了保障业务系统和网络信息数据不受来自用户的破坏，泄密，窃取',
},
{
  question: '下列各项中，哪一项不是文件型病毒的特点',
  answer: 'B',
  A: '病毒以某种形式隐藏在主程序中，并不修改主程序',
  B: '以自身逻辑部分取代合法的引导程序模块，导致系统瘫痪',
  C: '文件型病毒可以通过检查主程序的长度来判断其存在',
  D: '文件型病毒通常在运行主程序时进入内存',
},
{
  question: '哪些文件会被DOS病毒感染',
  answer: 'A',
  A: '可执行文件',
  B: '图形文件',
  C: '文本文件',
  D: '系统文件',
},
{
  question: '网络传播型木马的特征有很多，请问哪个描述是正确的',
  answer: 'B',
  A: '利用现实生活中的邮件进行散播，不会破坏数据，但是他将硬盘加密锁死',
  B: '兼备伪装和传播两种特征并结合TCP/IP网络技术四处泛滥，同时他还添加了“后门”和击键记录等功能',
  C: '通过伪装成一个合法性程序诱骗用户上当',
  D: '通过消耗内存而引起注意',
},
{
  question: '通过检查电子邮件信件和附件来查找某些特定的语句和词语、文件扩展名或病毒签名进行扫描是哪种扫描技术',
  answer: 'D',
  A: '实时扫描',
  B: '完整性扫描',
  C: '启发式扫描',
  D: '内容扫描',
},
{
  question: 'IPSec协议是开放的VPN协议。对它的描述有误的是',
  answer: 'C',
  A: '适应于向IPv6迁移',
  B: '提供在网络层上的数据加密保护',
  C: '支持动态的IP地址分配',
  D: '不支持除TCP/IP外的其它协议',
},
{
  question: '哪种类型的漏洞评估产品是可以模拟黑客行为，扫描网络上的漏洞并进行评估的',
  answer: 'A',
  A: '网络型安全漏洞评估产品',
  B: '主机型安全漏洞评估产品',
  C: '数据库安全漏洞评估产品',
  D: '以上皆是',
},
{
  question: '按感染对象分类，CIH病毒属于哪类病毒',
  answer: 'B',
  A: '引导区病毒',
  B: '文件型病毒',
  C: '宏病毒',
  D: '复合型病毒',
},
{
  question: '什么是网页挂马',
  answer: 'A',
  A: '攻击者通过在正常的页面中(通常是网站的主页)插入一段代码。 浏览者在打开该页面的时候，这段代码被执行，然后下载并运行某木马的服务器端程序，进而控制浏览者的主机',
  B: '黑客们利用人们的猎奇、贪心等心理伪装构造一个链接或者一个网页，利用社会工程学欺骗方法，引诱点击，当用户打开一个看似正常的页面时，网页代码随之运行，隐蔽性极高',
  C: '把木马服务端和某个游戏/软件捆绑成一个 文件通过QQ/MSN或邮件发给别人，或者通过制作BT木马种子进行快速扩散',
  D: '与从互联网上下我的免费游戏软件进行捆绑。被激活后，它就会将自己复制到Windows的系统文件夹中，并向注册表添加键值，保证它在启动时被执行。',
},
{
  question: '黑客利用IP地址进行攻击的方法有',
  answer: 'A',
  A: '欺骗',
  B: '解密',
  C: '窃取口令',
  D: '发送病毒',
},
{
  question: '以下哪一项属于基于主机的入侵检测方式的优势',
  answer: 'C',
  A: '监视整个网段的通信',
  B: '不要求在大量的主机上安装和管理软件',
  C: '适应交换和加密',
  D: '具有更好的实时性',
},
{
  question: '以下对特洛伊木马的概念描述正确的是',
  answer: 'B',
  A: '特洛伊木马不是真正的网络威胁，只是一种游戏',
  B: '特洛伊木马是指隐藏在正常程序中的一段具有特殊功能的恶意代码，是具备破坏和删除文件、发送密码、记录键盘和攻击Dos等特殊功能的后门程序。',
  C: '特洛伊木马程序的特征很容易从计算机感染后的症状上进行判断',
  D: '中了特洛伊木马就是指安装了木马的客户端程序，若你的电脑被安装了客户端程序，则拥有相应服务器端的人就可以通过网络控制你的电脑。',
},
{
  question: '以下对于反病毒技术的概念描述正确的是',
  answer: 'A',
  A: '提前取得计算机系统控制权，识别出计算机的代码和行为，阻止病毒取得系统控制权',
  B: '与病毒同时取得计算机系统控制权，识别出计算机的代码和行为，然后释放系统控制',
  C: '在病毒取得计算机系统控制权后，识别出计算机的代码和行为，然后释放系统控制权',
  D: '提前取得计算机系统控制权，识别出计算机的代码和行为，允许病毒取得系统控制权',
},
{
  quesiton: '我们将正在互联网上传播并正在日常的运行中感染者用户的病毒称为什么病毒',
  answer: 'C',
   A: '内存病毒',
   B: '隐密型病毒',
   C: '在野病毒',
   D: '多形态病毒',
},
];
