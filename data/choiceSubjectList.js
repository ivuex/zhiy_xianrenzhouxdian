export default [

  {
    type: 'choice',
    question: '一个数的最大因数与这个数的最小倍数',
    answer: 'A',	
    A: '相等',
    B: '不相等',
    C: '有时相等',
  },
  
  {
    type: 'choice',
    question: '一个西瓜的体积约是5',
    answer: 'B',	
    A: '立方米',
    B: '立方分米',
    C: '立方厘米',
  },

  
  {
    type: 'choice',
    question: '小光要统计今年1—6月份气温变化情况，用一下哪一项比较合适？',
    answer: 'B',	
    A: '扇形统计图',
    B: '折线统计图',
    C: '条形统计图',
  },
  
  {
    type: 'choice',
    question: '长方体的6个面展开后，以下哪一项是正确的？',
    answer: 'C',	
    A: '都是长方形',
    B: '至少有2个面是长方形',
    C: '至少有4个面是长方形',
  },

  {
    type: 'choice',
    question: '3个小正方体并排摆在空地上，露在外面的面有多少个？',
    answer: 'C',	
    A: '3个',
    B: '9个',
    C: '11个',
  },

  {
    type: 'choice',
    question: '棱长1分米的正方体玻璃缸，能容纳多大体积的液体？',
    answer: 'B',	
    A: '100mL',
    B: '1L',
    C: '1mL',
  },

  {
    type: 'choice',
    question: '小红爸爸今年35岁，妈妈比爸爸小1/7，妈妈今年多少岁？',
    answer: 'A',	
    A: '30',
    B: '42',
    C: '28',
  },

  {
    type: 'choice',
    question: '出勤率可能的区间范围是？',
    answer: 'C',	
    A: '大于100%',
    B: '小于100%',
    C: '小于或等于100%',
  },

  {
    type: 'choice',
    question: '？',
    answer: 'C',	
    A: '大于100%',
    B: '小于100%',
    C: '小于或等于100%',
  },

  {
    type: 'choice',
    question: '一个正方体切成两个大小相等的长方体后，表面积会怎样？',
    answer: 'A',	
    A: '增加',
    B: '减少',
    C: '不变',
  },
  {
    type: 'choice',
    question: 'a÷b = c（a，b都大于0），当b﹤1时，c和a会是怎样的关系？',
    answer: 'C',	
    A: 'c﹥a',
    B: 'c﹤a',
    C: 'c = a',
  },

];
